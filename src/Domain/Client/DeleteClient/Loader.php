<?php

namespace App\Domain\Client\DeleteClient;

use App\Domain\Common\Exceptions\ProcessorErrorsHttp;
use App\Domain\Common\Loader\AbstractLoader;
use App\Domain\Entity\Client;
use App\Domain\Repository\ClientRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use function is_null;

class Loader extends AbstractLoader
{
    /** @var ClientRepository $clientRepository */
    private $clientRepository;

    /** @var Security $security */
    private $security;

    public function __construct(
        ClientRepository $clientRepository,
        Security $security
    ) {
        $this->clientRepository = $clientRepository;
        $this->security = $security;
    }

    /**
     * @param Request $request
     * @return Client
     */
    protected function loadFromDatabase(Request $request): Client
    {
        $this->isValidUuid($request->attributes->get('clientUuid'));

        /** @var Client|null $client */
        $client = $this->clientRepository->find($request->attributes->get('clientUuid'));

        if (is_null($client)) {
            ProcessorErrorsHttp::throwNotFound(
                sprintf('Not found client with uuid: %s', $request->attributes->get('clientUuid'))
            );
        }

        if (!$this->security->isGranted('client_delete', $client)) {
            ProcessorErrorsHttp::throwAccessDenied('You are not allowed to access this resource');
        }

        return $client;
    }

    /**
     * @param mixed $date
     */
    protected function buildOutput($date): void
    {
        // TODO: Implement buildOutput() method.
    }

    /**
     * @param mixed $data
     */
    protected function sendDataFormatted($data): void
    {
        // TODO: Implement sendDataFormatted() method.
    }
}