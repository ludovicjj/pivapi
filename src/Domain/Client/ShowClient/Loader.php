<?php

namespace App\Domain\Client\ShowClient;

use App\Domain\Common\Exceptions\ProcessorErrorsHttp;
use App\Domain\Common\Helper\Hateoas\Links\AbstractLink;
use App\Domain\Common\Helper\Hateoas\Links\LinkBuilder;
use App\Domain\Common\Loader\AbstractLoader;
use App\Domain\Entity\Client;
use App\Domain\Repository\ClientRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use App\Domain\Common\Helper\Hateoas\HateoasBuilder;
use function is_null;

class Loader extends AbstractLoader
{
    /** @var ClientRepository $clientRepository */
    private $clientRepository;

    /** @var SerializerInterface $serializer */
    private $serializer;

    /** @var HateoasBuilder $hateoasBuilder */
    private $hateoasBuilder;

    /** @var AuthorizationCheckerInterface $authorization */
    private $authorization;

    public function __construct(
        ClientRepository $clientRepository,
        SerializerInterface $serializer,
        HateoasBuilder $hateoasBuilder,
        AuthorizationCheckerInterface $authorization
    ) {
        $this->clientRepository = $clientRepository;
        $this->serializer = $serializer;
        $this->hateoasBuilder = $hateoasBuilder;
        $this->authorization = $authorization;
    }

    /**
     * @param Request $request
     * @return Client
     */
    protected function loadFromDatabase(Request $request)
    {
        //TODO exception : invalid uuid
        $this->isValidUuid($request->attributes->get('clientUuid'));

        /** @var Client|null $client */
        $client = $this->clientRepository->find($request->attributes->get('clientUuid'));

        //TODO exception : not find resource
        if (is_null($client)) {
            ProcessorErrorsHttp::throwNotFound(
                sprintf(
                    "Not Found Client with uuid : %s", $request->attributes->get('clientUuid')
                )
            );
        }

        return $client;
    }

    /**
     * @param Client $data
     *
     * @return ClientItemOutput[]
     */
    protected function buildOutput($data)
    {
        $clientOut = new ClientItemOutput(
            $data->getId(),
            $data->getUsername(),
            $data->getFirstName(),
            $data->getLastName(),
            $data->getEmail(),
            $this->buildLinksHateoas($data)
        );

        $output = new ShowClientOutput();
        $output->addItem($clientOut);
        return $output->getItems();
    }

    /**
     * @param Client $client
     * @return array<string, AbstractLink>
     */
    private function buildLinksHateoas(Client $client): array
    {
        $links = [];
        $links[LinkBuilder::LIST] = $this->hateoasBuilder->build(
            LinkBuilder::LIST,
            'client_list'
        );
        if ($this->authorization->isGranted('ROLE_ADMIN')) {
            $links[LinkBuilder::DELETE] = $this->hateoasBuilder->build(
                LinkBuilder::DELETE,
                'client_delete',
                ['clientUuid' => $client->getId()]
            );
        }
        return $links;
    }

    /**
     * @param ClientItemOutput[] $data
     * @return string
     */
    protected function sendDataFormatted($data): string
    {
        return $this->serializer->serialize($data, 'json');
    }
}
