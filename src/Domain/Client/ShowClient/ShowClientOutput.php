<?php

namespace App\Domain\Client\ShowClient;


class ShowClientOutput
{
    /** @var ClientItemOutput[] */
    private $item;

    /**
     * @param ClientItemOutput $data
     */
    public function addItem($data): void
    {
        $this->item[] = $data;
    }

    /**
     * @return ClientItemOutput[]
     */
    public function getItems()
    {
        return $this->item;
    }
}