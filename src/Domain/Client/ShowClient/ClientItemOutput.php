<?php

namespace App\Domain\Client\ShowClient;

use App\Domain\Common\Helper\Hateoas\Links\AbstractLink;

class ClientItemOutput
{
    /** @var string $id */
    private $id;

    /** @var string $username */
    private $username;

    /** @var string $firstName */
    private $firstName;

    /** @var string $lastName */
    private $lastName;

    /** @var string $email */
    private $email;

    /** @var array<string, AbstractLink> $link */
    private $link;

    /**
     * ClientItemOutput constructor.
     * @param string $id
     * @param string $username
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param array<string, AbstractLink> $link
     */
    public function __construct(
        string $id,
        string $username,
        string $firstName,
        string $lastName,
        string $email,
        array $link
    ) {
        $this->id = $id;
        $this->username = $username;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return array<string, AbstractLink>
     */
    public function getLink(): array
    {
        return $this->link;
    }
}