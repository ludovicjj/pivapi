<?php

namespace App\Domain\Client\ListClient;


class ListClientOutput
{
    /**
     * @var ClientItemOutput[]|null
     */
    protected $items;

    /**
     * @return ClientItemOutput[]|null
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param ClientItemOutput $items
     */
    public function addItems(ClientItemOutput $items): void
    {
        $this->items[] = $items;
    }
}