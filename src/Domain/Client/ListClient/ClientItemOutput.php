<?php

namespace App\Domain\Client\ListClient;

use App\Domain\Common\Helper\Hateoas\Links\AbstractLink;

class ClientItemOutput
{
    /** @var string $id */
    private $id;

    /** @var string $username */
    private $username;

    /** @var string $email */
    private $email;

    /** @var array<string, AbstractLink> $links */
    private $links;

    /**
     * ClientItemOutput constructor.
     * @param string $id
     * @param string $username
     * @param string $email
     * @param array<string, AbstractLink> $links
     */
    public function __construct(
        string $id,
        string $username,
        string $email,
        array $links
    ) {
        $this->id = $id;
        $this->username = $username;
        $this->email = $email;
        $this->links = $links;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return array<string, AbstractLink>
     */
    public function getLinks(): array
    {
        return $this->links;
    }
}