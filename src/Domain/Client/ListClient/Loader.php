<?php

namespace App\Domain\Client\ListClient;

use App\Domain\Common\Helper\Hateoas\HateoasBuilder;
use App\Domain\Common\Helper\Hateoas\Links\AbstractLink;
use App\Domain\Common\Helper\Hateoas\Links\LinkBuilder;
use App\Domain\Common\Loader\AbstractLoader;
use App\Domain\Entity\Client;
use App\Domain\Repository\ClientRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

class Loader extends AbstractLoader
{
    /** @var ClientRepository $clientRepository */
    private $clientRepository;

    /** @var SerializerInterface $serializer */
    private $serializer;

    /** @var HateoasBuilder $hateoasBuilder */
    private $hateoasBuilder;

    /** @var Security $security */
    private $security;

    public function __construct(
        ClientRepository $clientRepository,
        SerializerInterface $serializer,
        HateoasBuilder $hateoasBuilder,
        Security $security
    )
    {
        $this->clientRepository = $clientRepository;
        $this->serializer = $serializer;
        $this->hateoasBuilder = $hateoasBuilder;
        $this->security = $security;
    }

    /**
     * @param Request $request
     * @return Client[]|array
     */
    protected function loadFromDatabase(Request $request)
    {
        return $this->clientRepository->listClient();
    }

    /**
     * @param ClientItemOutput[]|null $data
     * @return string|null
     */
    protected function sendDataFormatted($data): ?string
    {
        if (is_null($data)) {
            return null;
        }
        return $this->serializer->serialize($data, 'json');
    }

    /**
     * @param Client[]|array $data
     * @return ClientItemOutput[]|null
     */
    protected function buildOutput($data)
    {
        $output = new ListClientOutput();

        /** @var Client $client */
        foreach ($data as $client) {
            $clientOut = new ClientItemOutput(
                $client->getId(),
                $client->getUsername(),
                $client->getEmail(),
                $this->buildLinksHateoas($client)
            );
            $output->addItems($clientOut);
        }

        return $output->getItems();
    }

    /**
     * @param Client $client
     * @return array<string, AbstractLink>
     */
    private function buildLinksHateoas(Client $client): array
    {
        $links = [];

        //Todo add link show
        $links[LinkBuilder::SHOW_ONE] = $this->hateoasBuilder->build(
            LinkBuilder::SHOW_ONE,
            'client_show',
            ['clientUuid' => $client->getId()]
        );

        //Todo add link delete to only admin and owner of account
        if ($this->security->isGranted('client_delete', $client)) {
            $links[LinkBuilder::DELETE] = $this->hateoasBuilder->build(
                LinkBuilder::DELETE,
                'client_delete',
                ['clientUuid' => $client->getId()]
            );
        }

        return $links;
    }
}
