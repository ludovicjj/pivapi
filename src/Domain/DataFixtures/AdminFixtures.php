<?php

namespace App\Domain\DataFixtures;

use App\Domain\Entity\Admin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class AdminFixtures extends Fixture
{
    /** @var EncoderFactoryInterface $encoderFactory */
    private $encoderFactory;

    public function __construct(
        EncoderFactoryInterface $encoderFactory
    ) {
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {
        $data = [
            'username' => 'admin',
            'password' => 'admin'
        ];

        $admin = new Admin(
            $data['username'],
            $this->encoderFactory->getEncoder(Admin::class)->encodePassword($data['password'], '')
        );

        $manager->persist($admin);
        $manager->flush();
    }
}