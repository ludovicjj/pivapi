<?php

namespace App\Domain\DataFixtures;

use App\Domain\Common\Helper\TokenGenerator;
use App\Domain\Entity\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class ClientFixtures extends Fixture
{
    /** @var EncoderFactoryInterface */
    private $encoderFactory;

    public function __construct(
        EncoderFactoryInterface $encoderFactory
    ) {
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {
        $arrayData = [
            [
                "username" => 'client1',
                'password' => '123456',
                'email' => 'contact@client1.fr',
                'firstname' => 'John',
                'lastname' => 'doe'
            ],
            [
                "username" => 'client2',
                'password' => '123456',
                'email' => 'contact@client2.fr',
                'firstname' => 'Jane',
                'lastname' => 'doe'
            ]
        ];

        foreach ($arrayData as $data) {
            $client = new Client(
                $data['username'],
                $this->encoderFactory->getEncoder(Client::class)->encodePassword($data['password'], ''),
                $data['firstname'],
                $data['lastname'],
                $data['email'],
                TokenGenerator::generateToken()
            );

            $manager->persist($client);
        }

        $manager->flush();
    }
}
