<?php

namespace App\Domain\Account\Registration;

use App\Domain\Common\Exceptions\ValidatorException;
use App\Domain\Common\Factory\ErrorFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestResolver
{
    /** @var SerializerInterface $serializer */
    private $serializer;

    /** @var ValidatorInterface $validator */
    private $validator;

    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     *
     * @return RegistrationInput
     *
     * @throws ValidatorException
     */
    public function resolve(Request $request): RegistrationInput
    {
        /** @var RegistrationInput $input */
        $input = $this->serializer->deserialize($request->getContent(), RegistrationInput::class, 'json');

        $constraintList = $this->validator->validate($input);

        if (count($constraintList) > 0) {
            ErrorFactory::buildError($constraintList);
        }

        return $input;
    }
}
