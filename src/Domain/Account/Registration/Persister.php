<?php

namespace App\Domain\Account\Registration;

use App\Domain\Common\Factory\ClientFactory;
use App\Domain\Common\Helper\TokenGenerator;
use App\Domain\Entity\Client;
use App\Domain\InputInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class Persister
{
    /** @var EncoderFactoryInterface $encoderFactory */
    private $encoderFactory;

    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    public function __construct(
        EncoderFactoryInterface $encoderFactory,
        EntityManagerInterface $entityManager
    ) {
        $this->encoderFactory = $encoderFactory;
        $this->entityManager = $entityManager;
    }

    /**
     * @param InputInterface $input
     *
     * @throws \Exception
     */
    public function save(InputInterface $input): void
    {
        /** @var RegistrationInput $registrationInput */
        $registrationInput = $input;

        $client = ClientFactory::create(
            $registrationInput->getUsername(),
            $this->getEncoder()->encodePassword($registrationInput->getPassword(), ''),
            $registrationInput->getFistName(),
            $registrationInput->getLastName(),
            $registrationInput->getEmail(),
            TokenGenerator::generateToken()
        );

        $this->entityManager->persist($client);
        $this->entityManager->flush();
    }

    /**
     * @return PasswordEncoderInterface
     */
    private function getEncoder(): PasswordEncoderInterface
    {
        return $this->encoderFactory->getEncoder(Client::class);
    }
}