<?php

namespace App\Domain\Account\Registration;

use App\Domain\Common\Validator\UniqueEntityInput;
use App\Domain\InputInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class RegistrationInput
 * @UniqueEntityInput(
 *     class="App\Domain\Entity\Client",
 *     fields={"username", "email"}
 * )
 */
class RegistrationInput implements InputInterface
{
    /**
     * @var string|null
     *
     * @Assert\NotBlank(
     *     message="Le nom d'utilisateur est obligatoire."
     * )
     */
    protected $username;

    /**
     * @var string|null
     *
     * @Assert\NotBlank(
     *     message="Le mot de passe est obligatoire."
     * )
     */
    protected $password;

    /**
     * @var string|null
     *
     * @Assert\NotBlank(
     *     message="Le prénom est obligatoire."
     * )
     */
    protected $firstName;

    /**
     * @var string|null
     *
     * @Assert\NotBlank(
     *     message="Le nom de famille est obligatoire."
     * )
     */
    protected $lastName;


    /**
     * @var string|null
     *
     * @Assert\NotBlank(
     *     message="L'adresse email est obligatoire."
     * )
     */
    protected $email;

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getFistName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }
}
