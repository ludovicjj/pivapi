<?php
/**
 * Created by PhpStorm.
 * User: Ludovic
 * Date: 12/02/2020
 * Time: 09:31
 */

namespace App\Domain\Common\Helper\Hateoas\Links;


class LinkDelete extends AbstractLink
{
    protected $type = 'delete';

    protected $method = 'DELETE';
}