<?php
/**
 * Created by PhpStorm.
 * User: Ludovic
 * Date: 12/02/2020
 * Time: 09:36
 */

namespace App\Domain\Common\Helper\Hateoas\Links;


class LinkGetList extends AbstractLink
{
    protected $type = 'list';

    protected $method = 'GET';
}