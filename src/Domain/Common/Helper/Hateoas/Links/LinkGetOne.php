<?php
/**
 * Created by PhpStorm.
 * User: Ludovic
 * Date: 12/02/2020
 * Time: 09:35
 */

namespace App\Domain\Common\Helper\Hateoas\Links;


class LinkGetOne extends AbstractLink
{
    protected $type = 'self';

    protected $method = 'GET';
}