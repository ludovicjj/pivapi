<?php

namespace App\Domain\Common\Helper\Hateoas\Links;

class LinkPost extends AbstractLink
{
    protected $type = 'new';

    protected $method = 'POST';
}