<?php

namespace App\Domain\Common\Helper\Hateoas;

use App\Domain\Common\Helper\Hateoas\Links\AbstractLink;
use App\Domain\Common\Helper\Hateoas\Links\LinkBuilder;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class HateoasBuilder
{
    /** @var UrlGeneratorInterface */
    protected $urlGenerator;

    /**
     * HateoasBuilder constructor.
     *
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @param string $type
     * @param string $route
     * @param array<string, string> $params
     * @return AbstractLink
     */
    public function build(
        string $type,
        string $route,
        array $params = []
    ): AbstractLink {
        return LinkBuilder::build(
            $type,
            $this->urlGenerator->generate($route, $params, UrlGeneratorInterface::ABSOLUTE_URL)
        );
    }
}
