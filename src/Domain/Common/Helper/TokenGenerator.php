<?php

namespace App\Domain\Common\Helper;

class TokenGenerator
{
    /**
     * @return string
     */
    public static function generateToken(): string
    {
        return md5(uniqid());
    }
}
