<?php

namespace App\Domain\Common\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueEntityInput extends Constraint
{
    /** @var string */
    public $class;

    /** @var string[] $fields */
    public $fields = [];

    /** @var string */
    public $message = "{{ field }} est déjà {{ action }} par un autre utilisateur.";

    public function getRequiredOptions()
    {
        return [
            'class',
            'fields',
        ];
    }

    /**
     * @return string[]|string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}