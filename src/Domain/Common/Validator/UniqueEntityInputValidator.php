<?php

namespace App\Domain\Common\Validator;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_null;

class UniqueEntityInputValidator extends ConstraintValidator
{
    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof UniqueEntityInput) {
            throw new UnexpectedTypeException($constraint, UniqueEntityInput::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        $fields = $constraint->fields;
        foreach ($fields as $field) {
            $fieldValue = $value->{'get'.ucfirst($field)}();
            $object = $this->entityManager->getRepository($constraint->class)->findOneBy(
                [
                    $field => $fieldValue
                ]
            );

            if (!is_null($object)) {
                $message = $fieldValue;
                $action = "utilisé";
                if ($field === 'username') {
                    $message = "Ce nom d'utilisateur";
                } elseif ($field === "email") {
                    $message = "Cette adresse email";
                    $action .= "e";
                }

                $this->context
                    ->buildViolation($constraint->message)
                    ->setParameter('{{ field }}', $message)
                    ->setParameter('{{ action }}', $action)
                    ->atPath($field)
                    ->addViolation();
            }
        }
    }
}