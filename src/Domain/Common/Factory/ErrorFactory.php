<?php

namespace App\Domain\Common\Factory;

use App\Domain\Common\Exceptions\ValidatorException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ErrorFactory
{
    /**
     * @param ConstraintViolationListInterface<int, ConstraintViolationInterface> $constraintList
     * @throws ValidatorException
     */
    public static function buildError(ConstraintViolationListInterface $constraintList): void
    {
        $errors = [];

        /** @var ConstraintViolationInterface $constraint */
        foreach ($constraintList as $constraint) {
            $errors[$constraint->getPropertyPath()][] = $constraint->getMessage();
        }

        throw new ValidatorException(
            $errors,
            Response::HTTP_BAD_REQUEST
        );
    }
}
