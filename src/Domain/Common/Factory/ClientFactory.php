<?php

namespace App\Domain\Common\Factory;

use App\Domain\Entity\Client;

class ClientFactory
{
    /**
     * @param string $username
     * @param string $password
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $tokenActivation
     *
     * @return Client
     *
     * @throws \Exception
     */
    public static function create(
        string $username,
        string $password,
        string $firstName,
        string $lastName,
        string $email,
        string $tokenActivation
    ) {
        return new Client(
            $username,
            $password,
            $firstName,
            $lastName,
            $email,
            $tokenActivation
        );
    }
}
