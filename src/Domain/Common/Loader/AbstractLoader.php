<?php

namespace App\Domain\Common\Loader;

use App\Domain\Common\Exceptions\ProcessorErrorsHttp;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractLoader
{
    public function load(Request $request): ?string
    {
        $data = $this->loadFromDatabase($request);
        $output = $this->buildOutput($data);
        return $this->sendDataFormatted($output);
    }

    /**
     * @param string $uuid
     */
    protected function isValidUuid(string $uuid): void
    {
        if (!Uuid::isValid($uuid)) {
            ProcessorErrorsHttp::throwNotFound('Provide uuid with valid format');
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    abstract protected function loadFromDatabase(Request $request);

    /**
     * @param mixed $date
     * @return mixed
     */
    abstract protected function buildOutput($date);

    /**
     * @param mixed $data
     * @return mixed
     */
    abstract protected function sendDataFormatted($data);
}