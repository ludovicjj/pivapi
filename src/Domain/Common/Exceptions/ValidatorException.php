<?php

namespace App\Domain\Common\Exceptions;

class ValidatorException extends \Exception
{
    /** @var array<string, array> $errors */
    private $errors;

    /** @var int $statusCode */
    private $statusCode;

    /**
     * ValidatorException constructor.
     * @param int $statusCode
     * @param array<string, array> $errors
     */
    public function __construct(
        array $errors,
        int $statusCode
    )
    {
        $this->statusCode = $statusCode;
        $this->errors = $errors;
        parent::__construct();
    }

    /**
     * @return array<string, array>
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }
}
