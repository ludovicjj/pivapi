<?php

namespace App\Domain\Common\Exceptions;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProcessorErrorsHttp
{
    /**
     * @param string $message
     * @throws HttpException
     */
    public static function throwNotFound(string $message): void
    {
        throw new HttpException(
            Response::HTTP_NOT_FOUND,
            $message
        );
    }

    /**
     * @param string $message
     * @throws HttpException
     */
    public static function throwAccessDenied(string $message): void
    {
        throw new HttpException(
            Response::HTTP_FORBIDDEN,
            $message
        );
    }
}