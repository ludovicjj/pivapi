<?php

namespace App\Domain\Common\Voter;

use App\Domain\Entity\Client;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class ClientVoter extends Voter
{
    const DELETE = 'client_delete';

    /**
     * @inheritDoc
     */
    protected function supports($attribute, $subject): bool
    {
        if (!in_array($attribute, [self::DELETE])) {
            return false;
        }
        // only vote on Client objects inside this voter
        if (!$subject instanceof Client) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            // the user must be logged in; if not, deny access
            return false;
        }
        // you know $subject is a Client object, thanks to supports
        /** @var Client $client */
        $client = $subject;

        return $this->canDelete($client, $user);
    }

    /**
     * Authorise uniquement le propriétaire de l'account
     * ou un admin à acceder à cette ressouce
     *
     * @param Client $client
     * @param UserInterface $user
     * @return bool
     */
    private function canDelete(Client $client, UserInterface $user): bool
    {
        return $client === $user || in_array('ROLE_ADMIN', $user->getRoles());
    }
}