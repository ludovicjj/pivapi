<?php

namespace App\Domain\Common\Subscribers;

use App\Domain\Common\Exceptions\ValidatorException;
use App\Responder\ErrorResponder;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExceptionSubscriber implements EventSubscriberInterface
{
    /** @var ErrorResponder $errorResponder */
    private $errorResponder;

    public function __construct(
        ErrorResponder $errorResponder
    ) {
        $this->errorResponder = $errorResponder;
    }

    public static function getSubscribedEvents()
    {
        return [
            ExceptionEvent::class => 'onProcessException'
        ];
    }

    /**
     * @param ExceptionEvent $event
     */
    public function onProcessException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        switch (get_class($exception)) {
            case ValidatorException::class:
                $this->onProcessValidatorException($event);
                break;
            case HttpException::class:
                $this->onProcessHttpException($event);
                break;
        }
    }

    /**
     * @param ExceptionEvent $event
     */
    private function onProcessValidatorException(ExceptionEvent $event): void
    {
        /** @var ValidatorException $exception */
        $exception = $event->getThrowable();
        $event->setResponse(
            $this->errorResponder->response(
                $exception->getErrors(),
                $exception->getStatusCode()
            )
        );
    }

    /**
     * @param ExceptionEvent $event
     */
    private function onProcessHttpException(ExceptionEvent $event): void
    {
        /** @var HttpException $exception */
        $exception = $event->getThrowable();
        $event->setResponse(
            $this->errorResponder->response(
                [
                    'message' => $exception->getMessage(),
                    'code' => $exception->getStatusCode()
                ],
                $exception->getStatusCode()
            )
        );
    }
}
