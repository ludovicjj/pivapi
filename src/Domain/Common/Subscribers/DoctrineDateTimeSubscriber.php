<?php

namespace App\Domain\Common\Subscribers;

use App\Domain\Entity\AbstractEntity;
use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class DoctrineDateTimeSubscriber implements EventSubscriber
{
    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        /** @var AbstractEntity $entity */
        $entity = $args->getObject();

        if ($entity instanceof AbstractEntity) {
            $entity->onPersist();
        }
    }

    /**
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function preUpdate(LifecycleEventArgs $args): void
    {
        /** @var AbstractEntity $entity */
        $entity = $args->getObject();

        if ($entity instanceof AbstractEntity) {
            $entity->onUpdate();
        }
    }
}
