<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Admin;

class AdminRepository extends AbstractRepository
{
    public function getEntityClassName(): string
    {
        return Admin::class;
    }
}