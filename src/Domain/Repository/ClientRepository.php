<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Client;

/**
 * Class ClientRepository
 * @package App\Domain\Repository
 */
class ClientRepository extends AbstractRepository
{
    public function getEntityClassName(): string
    {
        return Client::class;
    }

    /**
     * @return array|Client[]
     */
    public function listClient()
    {
        $qb = $this->createQueryBuilder('c')
            ->getQuery();

        return $qb->getResult();
    }
}