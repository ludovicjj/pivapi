<?php

namespace App\Domain\Entity;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Admin
 * @ORM\Table(name="admin")
 * @ORM\Entity(repositoryClass="App\Domain\Repository\AdminRepository")
 *
 * @UniqueEntity(
 *     "username",
 *     message="Ce nom d'utilisateur est déjà utilisé."
 * )
 */
class Admin implements UserInterface
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $username;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var array<int, string>
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * Admin constructor.
     * @param string $username
     * @param string $password
     * @throws \Exception
     */
    public function __construct(
        string $username,
        string $password
    )
    {
        $this->id = Uuid::uuid4();
        $this->username = $username;
        $this->password = $password;
        $this->roles[] = 'ROLE_ADMIN';
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id->toString();
    }

    /**
     * @return array<int, string>
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @return string|null
     */
    public function getPassword()
    {
       return $this->password;
    }

    /**
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string The username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return null
     */
    public function eraseCredentials()
    {
        return null;
    }
}