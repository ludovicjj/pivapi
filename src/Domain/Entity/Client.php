<?php

namespace App\Domain\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="App\Domain\Repository\ClientRepository")
 *
 * @UniqueEntity(
 *     "username",
 *     message="Ce nom d'utilisateur est déjà utilisé."
 * )
 * @UniqueEntity(
 *     "email",
 *     message="Cette adresse email est déjà utilisé."
 * )
 */
class Client extends AbstractEntity implements UserInterface
{
    const STATUS_PENDING_ACTIVATION = 'pending_activation';
    const STATUS_ACTIVATED = 'activated';
    const STATUS_LOCK = 'locked';

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @var array<int, string>
     * @ORM\Column(type="array")
     */
    private $roles = [];

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $tokenActivation;

    /**
     * Client constructor.
     * @param string $username
     * @param string $password
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $tokenActivation
     *
     * @throws \Exception
     */
    public function __construct(
        string $username,
        string $password,
        string $firstName,
        string $lastName,
        string $email,
        string $tokenActivation
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->username = $username;
        $this->password = $password;
        $this->tokenActivation = $tokenActivation;
        $this->roles[] = 'ROLE_CLIENT';
        $this->status = self::STATUS_PENDING_ACTIVATION;
        parent::__construct();
    }

    /**
     * @return array<int, string>
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getTokenActivation(): string
    {
        return $this->tokenActivation;
    }

    /**
     * @return null
     */
    public function eraseCredentials()
    {
        return null;
    }
}