<?php

namespace App\Domain\Listeners\JWT;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Symfony\Component\HttpFoundation\JsonResponse;

class JWTNotFoundListener
{
    /**
     * @param JWTNotFoundEvent $event
     */
    public function onJWTNotFound(JWTNotFoundEvent $event): void
    {
        /*$data = [
            'status'  => '403 Forbidden',
            'message' => 'Veuillez fournir un token JWT',
        ];*/

        $data = [
            'status'  => '401 Unauthorized',
            'message' => 'Accès refusé, veuillez fournir un token JWT',
        ];

        $response = new JsonResponse($data, 401);

        $event->setResponse($response);
    }
}