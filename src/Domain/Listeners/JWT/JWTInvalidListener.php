<?php

namespace App\Domain\Listeners\JWT;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTInvalidEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;

class JWTInvalidListener
{
    /**
     * @param JWTInvalidEvent $event
     */
    public function onJWTInvalid(JWTInvalidEvent $event): void
    {
        /*$response = new JWTAuthenticationFailureResponse(
            'Your token is invalid, please login again to get a new one',
            403
        );*/

        $response = new JWTAuthenticationFailureResponse(
            'Votre token est invalide, reconnectez vous pour obtenir un token valide',
            401
        );

        $event->setResponse($response);
    }
}