<?php

namespace App\Responder;

use Symfony\Component\HttpFoundation\Response;
use function is_null;

class JsonResponder
{
    /**
     * @param string|null $data
     * @param int $statusCode
     * @param string[] $headers
     * @return Response
     */
    public static function response(
        ?string $data,
        int $statusCode = Response::HTTP_OK,
        array $headers = []
    ) {
        return new Response(
            $data,
            (!is_null($data)) ? $statusCode : Response::HTTP_NO_CONTENT,
            array_merge(
                $headers,
                ['Content-Type' => 'application/json']
            )
        );
    }
}
