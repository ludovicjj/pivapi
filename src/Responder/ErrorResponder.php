<?php

namespace App\Responder;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ErrorResponder
{
    /** @var SerializerInterface $serializer */
    private $serializer;

    public function __construct(
        SerializerInterface $serializer
    )
    {
        $this->serializer = $serializer;
    }

    /**
     * @param array<string, string|int|array> $data
     * @param int $statusCode
     * @return Response
     */
    public function response(array $data, int $statusCode = Response::HTTP_BAD_REQUEST): Response
    {
        return new Response(
            $this->serializer->serialize($data, 'json'),
            $statusCode,
            [
                'Content-Type' => 'application/json',
            ]
        );
    }
}