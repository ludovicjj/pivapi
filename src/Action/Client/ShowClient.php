<?php

namespace App\Action\Client;

use App\Domain\Client\ShowClient\Loader;
use App\Responder\JsonResponder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShowClient
{
    /** @var Loader $loader */
    private $loader;

    public function __construct(
        Loader $loader
    ) {
        $this->loader = $loader;
    }

    /**
     * @Route("/clients/{clientUuid}", name="client_show", methods={"GET"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function show(Request $request): Response
    {
        $output = $this->loader->load($request);
        return JsonResponder::response($output);
    }
}