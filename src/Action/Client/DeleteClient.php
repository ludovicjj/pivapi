<?php

namespace App\Action\Client;

use App\Domain\Client\DeleteClient\Loader;
use App\Responder\JsonResponder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeleteClient
{
    /** @var Loader $loader */
    private $loader;

    public function __construct(
        Loader $loader
    ) {
        $this->loader = $loader;
    }

    /**
     * @Route("/clients/{clientUuid}", name="client_delete", methods={"DELETE"})
     *
     * @param Request $request
     * @return Response
     */
    public function delete(Request $request): Response
    {
        $this->loader->load($request);

        $data = [
          'message' => 'You are allow to continue',
          'code' => 200
        ];

        return JsonResponder::response(json_encode($data));
    }
}