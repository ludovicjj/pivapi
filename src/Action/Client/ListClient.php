<?php

namespace App\Action\Client;

use App\Domain\Client\ListClient\Loader;
use App\Responder\JsonResponder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListClient
{
    /** @var Loader $loader */
    private $loader;

    public function __construct(
        Loader $loader
    ) {
        $this->loader = $loader;
    }

    /**
     * @Route("/clients", name="client_list", methods={"GET"})
     *
     * @param Request $request
     * @return Response
     */
    public function list(Request $request): Response
    {
        $output = $this->loader->load($request);
        return JsonResponder::response($output);
    }
}