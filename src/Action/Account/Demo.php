<?php

namespace App\Action\Account;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Demo
{
    /**
     * @Route("/accounts/message", name="account_test", methods={"GET"})
     * @return Response
     */
    public function index(): Response
    {
        $data = [
            'prenom' => 'Ludovic',
            'nom' => 'Jahan',
            'role' => 'dev',
            'message' => "J'essaye d'envoyer un json à un autre controller de mon application"
        ];

        $json = json_encode($data);
        return new Response($json, 200, ['Content-Type' => 'application/json']);
    }
}