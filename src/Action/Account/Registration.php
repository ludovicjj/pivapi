<?php

namespace App\Action\Account;

use App\Domain\Account\Registration\Persister;
use App\Domain\Account\Registration\RequestResolver;
use App\Domain\Common\Exceptions\ValidatorException;
use App\Responder\ErrorResponder;
use App\Responder\JsonResponder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Registration
{
    /** @var RequestResolver $resolver */
    private $resolver;

    /** @var Persister $persister */
    private $persister;

    /** @var ErrorResponder $errorResponder */
    private $errorResponder;

    public function __construct(
        RequestResolver $resolver,
        Persister $persister,
        ErrorResponder $errorResponder
    ) {
        $this->resolver = $resolver;
        $this->persister = $persister;
        $this->errorResponder = $errorResponder;
    }

    /**
     * @Route("/accounts", name="account_registration", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ValidatorException
     * @throws \Exception
     *
     */
    public function registration(Request $request)
    {
        $result = $this->isJson($request->getContent());
        if (!$result) {
            return $this->errorResponder->response(
                ["message" => "invalid json"],
                Response::HTTP_BAD_REQUEST
            );
        }

        $input = $this->resolver->resolve($request);

        $this->persister->save($input);

        return JsonResponder::response(
            json_encode(
                [
                    'message' => "La ressource a été ajoutée",
                    'code' => 201
                ]
            ),
            201
        );
    }

    /**
     * Test if request contain a valid json
     *
     * @link https://stackoverflow.com/questions/6041741/fastest-way-to-check-if-a-string-is-json-in-php
     *
     * @param string $body
     * @return bool
     */
    private function isJson(string $body): bool
    {
        json_decode($body);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}