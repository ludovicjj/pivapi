<?php

namespace App\Tests\Action\Account;

use App\Domain\DataFixtures\ClientFixtures;
use App\Tests\DataBaseTestCase;
use Symfony\Component\HttpFoundation\Response;

class RegistrationTest extends DataBaseTestCase
{
    public function testRegistrationSuccess()
    {
        $data = [
            'username' => 'demo',
            'firstName' => 'john',
            'lastName' => 'doe',
            'email' => 'contact@doe.fr',
            'password' => '123456'
        ];

        $json = json_encode($data);

        $this->kernelBrowser->request('POST', '/api/accounts', [], [], [], $json);
        $this->assertEquals(Response::HTTP_CREATED, $this->kernelBrowser->getResponse()->getStatusCode());
        $this->assertTrue($this->kernelBrowser->getResponse()->headers->contains('content-type', 'application/json'));
    }

    public function testRegistrationWithMissingProperty()
    {
        $data = [
            'firstName' => 'john',
            'lastName' => 'doe',
            'email' => 'contact@doe.fr',
            'password' => '123456'
        ];

        $json = json_encode($data);

        $this->kernelBrowser->request('POST', '/api/accounts', [], [], [], $json);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $this->kernelBrowser->getResponse()->getStatusCode());
        $this->assertTrue($this->kernelBrowser->getResponse()->headers->contains('content-type', 'application/json'));
    }

    public function testRegistrationUniqueEntityConstraintViolation()
    {
        $this->loadFixtures([ClientFixtures::class]);

        $data = [
            "username" => 'client1',
            'password' => '123456',
            'email' => 'contact@client1.fr',
            'firstName' => 'john',
            'lastName' => 'doe'
        ];

        $json = json_encode($data);

        $this->kernelBrowser->request('POST', '/api/accounts', [], [], [], $json);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $this->kernelBrowser->getResponse()->getStatusCode());
        $this->assertTrue($this->kernelBrowser->getResponse()->headers->contains('content-type', 'application/json'));
    }
}