<?php

namespace App\Tests\Action\Client;

use App\Domain\DataFixtures\AdminFixtures;
use App\Domain\DataFixtures\ClientFixtures;
use App\Domain\Entity\Client;
use App\Domain\Repository\ClientRepository;
use App\Tests\DataBaseTestCase;
use Symfony\Component\HttpFoundation\Response;

class ListClientTest extends DataBaseTestCase
{
    public function testListClientUri()
    {
        $client = $this->createAuthenticatedAdmin(
            'admin',
            'admin',
            [
                AdminFixtures::class,
                ClientFixtures::class
            ]
        );

        $client->request('GET', '/api/clients');
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('content-type', 'application/json'));
    }

    public function testListClientUriWithoutClient()
    {
        $client = $this->createAuthenticatedAdmin(
            'admin',
            'admin',
            [AdminFixtures::class]
        );

        $client->request('GET', '/api/clients');
        $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());
    }

    public function testCountListClientUri()
    {
        $client = $this->createAuthenticatedAdmin(
            'admin',
            'admin',
            [
                AdminFixtures::class,
                ClientFixtures::class
            ]
        );
        $client->request('GET', '/api/clients');

        $data = json_decode($client->getResponse()->getContent(), true);

        /** @var ClientRepository $clientRepository */
        $clientRepository = $this->entityManager->getRepository(Client::class);

        $this->assertCount(2, $data);
        $this->assertSame($clientRepository->count([]), count($data));
    }
}