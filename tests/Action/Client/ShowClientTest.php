<?php

namespace App\Tests\Action\Client;

use App\Domain\DataFixtures\AdminFixtures;
use App\Domain\DataFixtures\ClientFixtures;
use App\Domain\Entity\Client;
use App\Domain\Repository\ClientRepository;
use App\Tests\DataBaseTestCase;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ShowClientTest extends DataBaseTestCase
{
    private const VALID_FORMAT_UUID = 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa';
    private const INVALID_FORMAT_UUID = 'aaaa-aaaa-aaaa-aaaa-aaaaaa';

    /** @var ClientRepository $clientRepository */
    private $clientRepository;

    /** @var Router|null $router */
    private $router;

    public function setUp()
    {
        parent::setUp();
        $this->clientRepository = $this->entityManager->getRepository(Client::class);
        $this->router = self::$container->get('router');
    }

    public function testShowClientUri()
    {
        $kernelBrowser = $this->createAuthenticatedAdmin(
            'admin',
            'admin',
            [
                AdminFixtures::class,
                ClientFixtures::class
            ]
        );

        /** @var Client $client */
        foreach ($this->clientRepository->findAll() as $client) {
            $kernelBrowser->request('GET', '/api/clients/' . $client->getId());
            $this->assertEquals(Response::HTTP_OK, $kernelBrowser->getResponse()->getStatusCode());
            $this->assertTrue(
                $this->kernelBrowser->getResponse()->headers->contains('content-type', 'application/json')
            );
        }
    }

    public function testShowClientUriResponseContent()
    {
        $kernelBrowser = $this->createAuthenticatedAdmin(
            'admin',
            'admin',
            [
                AdminFixtures::class,
                ClientFixtures::class
            ]
        );

        /** @var Client|null $client */
        $client = $this->clientRepository->findOneBy(['username' => 'client1']);

        $kernelBrowser->request('GET', '/api/clients/' . $client->getId());
        $response = json_decode($kernelBrowser->getResponse()->getContent(), true)[0];

        $this->assertEquals($client->getId(), $response['id']);
        $this->assertEquals($client->getUsername(), $response['username']);
        $this->assertEquals($client->getFirstName(), $response['firstName']);
        $this->assertEquals($client->getLastName(), $response['lastName']);
        $this->assertEquals($client->getEmail(), $response['email']);
        $this->assertEquals(
            $this->router->generate('client_list', [], UrlGeneratorInterface::ABSOLUTE_URL),
            $response['link']['list']['href']
        );
        $this->assertEquals(
            $this->router->generate(
                'client_delete',
                ['clientUuid' => $client->getId()],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
            $response['link']['delete']['href']
        );
    }

    public function testShowClientUriWithInvalidFormatUuid()
    {
        $kernelBrowser = $this->createAuthenticatedAdmin(
            'admin',
            'admin',
            [
                AdminFixtures::class,
                ClientFixtures::class
            ]
        );

        $kernelBrowser->request('GET', '/api/clients/' . self::INVALID_FORMAT_UUID);
        $response = json_decode($kernelBrowser->getResponse()->getContent(), true);

        $this->assertEquals(Response::HTTP_NOT_FOUND, $kernelBrowser->getResponse()->getStatusCode());
        $this->assertEquals('Provide uuid with valid format', $response['message']);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response['code']);
    }

    public function testShowClientUriWithValidUuid()
    {
        $kernelBrowser = $this->createAuthenticatedAdmin(
            'admin',
            'admin',
            [
                AdminFixtures::class,
                ClientFixtures::class
            ]
        );

        $kernelBrowser->request('GET', '/api/clients/' . self::VALID_FORMAT_UUID);
        $response = json_decode($kernelBrowser->getResponse()->getContent(), true);

        $this->assertEquals(Response::HTTP_NOT_FOUND, $kernelBrowser->getResponse()->getStatusCode());
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response['code']);
        $this->assertEquals(
            'Not Found Client with uuid : aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa',
            $response['message']
        );
    }
}