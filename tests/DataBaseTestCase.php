<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Doctrine\ORM\Tools\ToolsException;

class DataBaseTestCase extends WebTestCase
{
    use FixturesTrait;

    /** @var ContainerInterface $container */
    protected static $container = null;

    /** @var SchemaTool $schemaTool */
    protected $schemaTool;

    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var Application */
    protected static $application = null;

    /** @var KernelBrowser */
    protected $kernelBrowser;

    /**
     * @throws ToolsException
     */
    public function setUp()
    {
        $this->kernelBrowser = self::createClient();
        self::$container = $this->kernelBrowser->getContainer();
        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->schemaTool = new SchemaTool($this->entityManager);
        $this->schemaTool->dropSchema($this->entityManager->getMetadataFactory()->getAllMetadata());
        $this->schemaTool->createSchema($this->entityManager->getMetadataFactory()->getAllMetadata());
    }

    public function tearDown()
    {
        //  ensure the kernel is shut down before calling the method
        parent::tearDown();
        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }

    /**
     * @return Application
     */
    protected static function getApplication()
    {
        if (null === self::$application) {
            $client = static::createClient();

            self::$application = new Application($client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }

    /**
     * @param $command
     *
     * @throws \Exception
     */
    protected function runFixtures($command)
    {
        $output = new \Symfony\Component\Console\Output\NullOutput();
        $this->getApplication()->run(new StringInput($command), $output);
    }

    /**
     * Test if new Client entity has errors
     *
     * @param $entity
     * @param int $expectedErrors
     */
    protected function isEntityValid($entity, int $expectedErrors)
    {
        $errors = self::$container->get('validator')->validate($entity);

        $message = [];
        /** @var ConstraintViolation $error */
        foreach ($errors as $error) {
            $message[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($expectedErrors, $errors, implode(', ', $message));
    }

    /**
     * Log with ADMIN_ROLE
     *
     * @param string $username
     * @param string $password
     * @param array $fixtures
     *
     * @return KernelBrowser
     */
    protected function createAuthenticatedAdmin(string $username, string $password, array $fixtures = [])
    {
        $this->loadFixtures($fixtures);

        $this->kernelBrowser->request(
            'POST',
            '/api/login_check',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'username' => $username,
                'password' => $password,
            ))
        );

        $data = json_decode($this->kernelBrowser->getResponse()->getContent(), true);

        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));

        return $client;
    }
}
