<?php

namespace App\Tests\Domain\Common\Helper\Hateoas;

use App\Domain\Common\Helper\Hateoas\HateoasBuilder;
use App\Domain\Common\Helper\Hateoas\Links\AbstractLink;
use App\Domain\Common\Helper\Hateoas\Links\LinkBuilder;
use App\Domain\Entity\Client;
use App\Domain\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class HateoasBuilderTest extends KernelTestCase
{
    /** @var Router|null  */
    private $router;

    /** @var HateoasBuilder $hateoasBuilder */
    private $hateoasBuilder;

    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    public function setUp()
    {
        self::bootKernel();
        $this->router = self::$container->get('router');
        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->hateoasBuilder = new HateoasBuilder(
            $this->router->getGenerator()
        );
    }

    public function tearDown()
    {
        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testBuild(): void
    {
        $abstractLink = $this->hateoasBuilder->build(
            LinkBuilder::LIST,
            'client_list',
            []
        );

        $this->assertInstanceOf(AbstractLink::class, $abstractLink);
        $this->assertEquals('GET', $abstractLink->getMethod());
        $this->assertEquals('http://localhost/api/clients', $abstractLink->getHref());
    }

    public function testBuildWithParams(): void
    {
        /** @var ClientRepository $clientRepo */
        $clientRepo = $this->entityManager->getRepository(Client::class);

        /** @var Client|null $client */
        $client = $clientRepo->findOneBy(['username' => 'client1']);

        $abstractLink = $this->hateoasBuilder->build(
            LinkBuilder::SHOW_ONE,
            'client_show',
            ['clientUuid' => $client->getId()]
        );

        $this->assertInstanceOf(AbstractLink::class, $abstractLink);
        $this->assertEquals('GET', $abstractLink->getMethod());
        $this->assertEquals("http://localhost/api/clients/{$client->getId()}", $abstractLink->getHref());
    }
}