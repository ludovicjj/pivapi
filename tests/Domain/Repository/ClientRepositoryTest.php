<?php

namespace App\Tests\Domain\Repository;

use App\Domain\DataFixtures\ClientFixtures;
use App\Domain\Repository\ClientRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ClientRepositoryTest extends KernelTestCase
{
    use FixturesTrait;

    public function testCount()
    {
        self::bootKernel();

        $this->loadFixtures([ClientFixtures::class]);
        $clients = self::$container->get(ClientRepository::class)->count([]);
        $this->assertEquals(2, $clients);
    }
}