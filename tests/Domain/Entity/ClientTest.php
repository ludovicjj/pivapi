<?php

namespace App\Tests\Domain\Entity;

use App\Domain\DataFixtures\ClientFixtures;
use App\Domain\Entity\Client;
use App\Tests\DataBaseTestCase;

class ClientTest extends DataBaseTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @param string $username
     * @param string $password
     * @param string $email
     *
     * @return Client
     * @throws \Exception
     */
    private function makeClient(string $username, string $password, string $email)
    {
        return new Client(
            $username,
            $password,
            'john',
            'doe',
            $email,
            md5(uniqid())
        );
    }

    /**
     * @throws \Exception
     */
    public function testValidClientEntityWithoutFixtures()
    {
        $client = $this->makeClient('client1', '123456', 'contact@client1.fr');
        $this->isEntityValid($client, 0);
    }

    /**
     * @throws \Exception
     */
    public function testConstraintUniqueEntityUsername()
    {
        $this->loadFixtures([ClientFixtures::class]);
        $client = $this->makeClient('client1', '123456', 'contact@test.fr');
        $this->isEntityValid($client, 1);
    }

    /**
     * @throws \Exception
     */
    public function testConstraintUniqueEntityEmail()
    {
        $this->loadFixtures([ClientFixtures::class]);
        $client = $this->makeClient('test', '123456', 'contact@client1.fr');
        $this->isEntityValid($client, 1);
    }
}
