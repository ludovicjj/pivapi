<?php

namespace App\Tests\Domain\Entity;

use App\Domain\DataFixtures\AdminFixtures;
use App\Domain\Entity\Admin;
use App\Tests\DataBaseTestCase;

class AdminTest extends DataBaseTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @param string $username
     * @param string $password
     * @return Admin
     * @throws \Exception
     */
    private function makeAdmin(string $username, string $password)
    {
        return new Admin(
            $username,
            $password
        );
    }

    /**
     * @throws \Exception
     */
    public function testValidAdminEntityWithoutFixtures()
    {
        $client = $this->makeAdmin('admin', 'admin');
        $this->isEntityValid($client, 0);
    }

    /**
     * @throws \Exception
     */
    public function testConstraintUniqueEntityUsername()
    {
        $this->loadFixtures([AdminFixtures::class]);
        $client = $this->makeAdmin('admin', 'admin');
        $this->isEntityValid($client, 1);
    }
}